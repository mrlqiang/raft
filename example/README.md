
# cli.py的用法

需要把cli.py与server放到同一个目录中。

## init
 * init

 初始化raft数据存放目录，默认路径是 /tmp/raft。这个命令删除/tmp/raft里面的所
 有内容，当重启raft进程的时候，这个进程作为新进程启动。如果不删除这个目录，
 重启raft进程，启动之后，会首先尝试从这个目录中装入数据。

## fork
 * fork <id>

 创建一个新的raft进程，raft id为命令行中的id。

## kill
 * kill <id>

 杀掉一个已经存在的raft进程。

## list
 * list

 列出所有已经存在的raft进程。

## conn
 * conn <id>

 链接到一个raft进程上，与这个raft进程进行通信；这个命令如果执行成功，就会进入
 raft进程链接状态，命令行提示符将变为 raft-(id)>。键入 'q'可以退出进程链接状
 态。

 下面这些命令是在raft进程内执行的，只对单个raft进程起作用。

### raft
 * raft

 这个命令列出本raft进程的基本信息，例如：id，状态(leader/follower/candidate)，
 角色(spare/standby/voter)等。

### raft conf
 * raft conf

 这个命令列出当前raft进程的配置信息。


### raft conf add
 * raft conf add <id>

 给raft配置增加一个成员，这个命令只有在本raft进程是leader的情况下才能执行成功。

### raft conf del
 * raft conf del <id>

 更改配置，删除一个成员，只有Leader才能执行成功。

### raft conf role
 * raft conf role <id> <spare|standby|voter>

 改配置，更改成员角色。只有Leader才能执行成功。

### raft conf replace
 * raft conf replace <old_id> <new_id> 

 替换一个成员，只有leader才能成功。
 注：尚未实现。

